package s0542718_s0546568;

public class Obstacle {
	public double radius;
	public Vector emphasis;
	
	public Obstacle(double radius, Vector emphasis) {
		this.radius = radius;
		this.emphasis = emphasis;
	}	
	
	public Obstacle(double radius, Vector[] vertices) {
		this.radius = radius;
		this.emphasis = calculateEmphasis(vertices);
	}
	
	public Obstacle(Vector[] vertices) {		
		this.emphasis = calculateEmphasis(vertices);
		this.radius = calcRadius(vertices);
	}
	
	public Obstacle(int[] xPoints, int[] yPoints) {
		Vector[] vertices = new Vector[xPoints.length];
		for(int i = 0; i<xPoints.length;i++){
			vertices[i]=new Vector(xPoints[i], yPoints[i]);
		}
		this.emphasis = calculateEmphasis(vertices);
		this.radius = calcRadius(vertices);
	}	

	private double calcRadius(Vector[] vertices) {
		double radius=0;
		for(int i = 0; i<vertices.length;i++){
			double distance = Math.sqrt((vertices[i].dX-this.emphasis.dX)*(vertices[i].dX-this.emphasis.dX)+(vertices[i].dY-this.emphasis.dY)*(vertices[i].dY-this.emphasis.dY));
			if(distance>radius){
				radius=distance;
			}
		}
		return radius;
	}

	private static Vector calculateEmphasis(Vector[] vertices) {
		double area = calcArea(vertices);
		double xs = 0;
		double ys = 0;
		for(int i = 0; i<vertices.length;i++){
			xs+= (vertices[i].dX+vertices[(i+1)%vertices.length].dX) * ( vertices[i].dX*vertices[(i+1)%vertices.length].dY -  vertices[(i+1)%vertices.length].dX*vertices[i].dY);
			ys+= (vertices[i].dY+vertices[(i+1)%vertices.length].dY) * ( vertices[i].dX*vertices[(i+1)%vertices.length].dY -  vertices[(i+1)%vertices.length].dX*vertices[i].dY);
		}	
		xs/=6*area;
		ys/=6*area;
		return new Vector(xs,ys);
	}
	
	private static double calcArea(Vector[] vertices){
		double area=0;
		for(int i = 0; i<vertices.length;i++){
			area+=(vertices[i].dX*vertices[(i+1)%vertices.length].dY-vertices[(i+1)%vertices.length].dX*vertices[i].dY);
		}
		return (area/2);
	}
	
//	public static void main(String[] args) {
//		Vector[]points = new Vector[4];
//		points[0]=new Vector(0,0);
//		points[1]=new Vector(0,3);
//		points[2]=new Vector(2,3);
//		points[3]=new Vector(2,0);
//		Obstacle o = new Obstacle(points);
//		System.out.println(o.radius);
//		System.out.println(o.emphasis.dX);System.out.println(o.emphasis.dY);
//	}
}
