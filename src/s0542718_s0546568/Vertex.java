package s0542718_s0546568;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Vertex implements Comparable<Vertex>{
	
	public double x;
	public double y;
	public List<Vertex> neighbors;
	public boolean isInFreespace;
	public boolean isInFastZone;
	public boolean isInSlowZone;
	public double cost = Double.MAX_VALUE;
	public double tempCostForDyksta = 0;
	public Vertex pointerToPrevious;
	
	/**
	 * @param x
	 * @param y
	 */
	public Vertex(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.neighbors=new ArrayList<Vertex>();
	}
	
	public void addNeighbor(Vertex v){
		this.neighbors.add(v);
	}
	
	public void resetForDijkstra(){
		this.tempCostForDyksta=0;
		this.pointerToPrevious=null;
	}

	@Override
	public int compareTo(Vertex arg0) {
		if(this.tempCostForDyksta<arg0.tempCostForDyksta){
			return -1;
		}
		if(this.tempCostForDyksta>arg0.tempCostForDyksta){
			return 1;
		}
		return 0;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Vertex - X: "+x+" Y: "+y;
	}
	
}
