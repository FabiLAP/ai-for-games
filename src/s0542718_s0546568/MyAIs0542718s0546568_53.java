package s0542718_s0546568;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

import org.lwjgl.opengl.GL11;

import lenz.htw.ai4g.ai.AI;
import lenz.htw.ai4g.ai.Acceleration;
import lenz.htw.ai4g.ai.Info;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Stack;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

public class MyAIs0542718s0546568_53 extends AI {
	
	final static String NAME = "EY AI 5.3";
	
	final static double ZIELRADIUS = 0.01;
	final static double ABBREMSRADIUS = 20;	
	final static double WUNSCHABBREMSZEIT = 1.5;
	
	final static double TOLERANZWINKEL = 0.0001;
	final static double ABBREMSWINKEL = 0.5;		
	final static double WUNSCHDREHZEIT = 0.1;		
	
	final static double BORDERANGLEFORSLOWDOWN = Math.PI/8;
	final static double MINVELOCITYFORLOWDOWN = 10;
	final static double MAXANGLEBETWEENVELOCITYANDDESTFORSLOWDOWN = Math.PI/3;
	
	final static double RADIUSFORGETTINGNEXTREACHABLEVERTEX = 100;	
			
	static double WidthForOptimization = 15;
	static double WidthForLookAhead = 6;
	int cellDim = 35;
	
	Vector vecOrientation;
	Vector vecDestination;		
	double angleBetweenOrientationAndDestination;	
	double orientationDestination;	
	boolean initialised = false;	
	Obstacle[] obstacles;	
	double calculatedAcc; //returnvalue
	double calculatedRotAcc; //returnvalue
	java.awt.Point lastCheckpoint;	
	Area[] freeSpaceAreas;
	Area[] slowZoneAreas;
	Area[] fastZoneAreas;
	Vertex[][] vertices;
	Stack<Vertex> pathToDest;
	Vertex myPositionVertex;
	Vertex checkpointVertex;
	Vertex currentDestination;
	boolean goStraight = false;
	
	double toX = 0;
	double toY = 0;
	
	@Override
	public boolean isEnabledForRacing() {
		// TODO Auto-generated method stub
		return false;
	}

	public MyAIs0542718s0546568_53(Info info) {
		super(info);
		initAreas();
		createVertexMatrix();
		lastCheckpoint= new Point(info.getCurrentCheckpoint());
		updateMyPositionVertex();
		updateCheckpointAsVertex();
//		pathToDest=findPath( myPositionVertex, checkpointVertex);
//		currentDestination = pathToDest.peek();
	}
	
	public String getName() {
		return NAME;	
	}	

	public String getTextureResourceName() {
		return "/s0542718_s0546568/Body.png";
	}
	
	void initAreas(){
		freeSpaceAreas = new Area[info.getTrack().getObstacles().length];
		for(int i = 0; i<freeSpaceAreas.length; i++){
			freeSpaceAreas[i]=new Area(info.getTrack().getObstacles()[i]);
		}
		slowZoneAreas = new Area[info.getTrack().getSlowZones().length];
		for(int i = 0; i<slowZoneAreas.length; i++){
			slowZoneAreas[i]=new Area(info.getTrack().getSlowZones()[i]);
		}
		fastZoneAreas = new Area[info.getTrack().getFastZones().length];
		for(int i = 0; i<fastZoneAreas.length; i++){
			fastZoneAreas[i]=new Area(info.getTrack().getFastZones()[i]);
		}
		
	}
	
	private Vertex getNextVertexInFreespace(double x, double y){
		int xIndex = (int) (x/cellDim);
		int yIndex = (int) (y/cellDim);
		Vertex toReturn = null;
		int dist = 1;
		while(toReturn==null){
			try {
				toReturn = vertices[xIndex][yIndex];
				if(toReturn.isInFreespace)return toReturn;
			} catch (Exception e0) {
				try {
					toReturn = vertices[xIndex+1*dist][yIndex];
					if(toReturn.isInFreespace)return toReturn;
				} catch (Exception e1) {
					try {
						toReturn = vertices[xIndex][yIndex+1*dist];
						if(toReturn.isInFreespace)return toReturn;
					} catch (Exception e2) {
						try {
							toReturn = vertices[xIndex-1*dist][yIndex];
							if(toReturn.isInFreespace)return toReturn;
						} catch (Exception e3) {
							try {
								toReturn = vertices[xIndex][yIndex-1*dist];
								if(toReturn.isInFreespace)return toReturn;
							} catch (Exception e4) {
								try {
									toReturn = vertices[xIndex+1*dist][yIndex+1*dist];
									if(toReturn.isInFreespace)return toReturn;
								} catch (Exception e5) {
									try {
										toReturn = vertices[xIndex-1*dist][yIndex-1*dist];
										if(toReturn.isInFreespace)return toReturn;
									} catch (Exception e6) {
										try {
											toReturn = vertices[xIndex+1*dist][yIndex-1*dist];
											if(toReturn.isInFreespace)return toReturn;
										} catch (Exception e7) {
											try {
												toReturn = vertices[xIndex-1*dist][yIndex+1*dist];
												if(toReturn.isInFreespace)return toReturn;
											} catch (Exception e8) {
												//Yes i know - hotfix!
											}
										}
									}
								}
							}
						}
					}
				}
			}
			dist++;
		}
//		System.out.println("returned - "+toReturn);
		return toReturn;
	}
	
	private void updateMyPositionVertex(){
		myPositionVertex = getNextVertexInFreespace(info.getX(),info.getY());
	}
	
	private void updateCheckpointAsVertex(){		
		if (info.getCurrentCheckpoint()!=null) {
			checkpointVertex = getNextVertexInFreespace(info.getCurrentCheckpoint().x,info.getCurrentCheckpoint().y);
		}
		else{
			checkpointVertex = vertices[25][25];
		}
	}

	private void createVertexMatrix() {		
		vertices = new Vertex[info.getTrack().getWidth() / cellDim][info.getTrack().getHeight() / cellDim];
		for (int y = 0; y < vertices[0].length; y++) {
			for (int x = 0; x < vertices.length; x++) {
				vertices[x][y]=new Vertex(x*cellDim+cellDim/2, y*cellDim+cellDim/2);
				if (isVertexAreaInFreespace(vertices[x][y].x, vertices[x][y].y)) {
					vertices[x][y].isInFreespace = true;
					vertices[x][y].cost = 1;
				}				
				if (isVertexAreaInFastZone(vertices[x][y].x, vertices[x][y].y)){
					vertices[x][y].isInFastZone = true;
					vertices[x][y].cost = 0.25;
				}
				if (isVertexAreaInSlowZone(vertices[x][y].x, vertices[x][y].y)){
					vertices[x][y].isInSlowZone = true;
					vertices[x][y].cost = 4;
				}
			}
		}		
		for (int y = 0; y < vertices[0].length; y++) {
			for (int x = 0; x < vertices.length; x++) {				
				if (vertices[x][y].isInFreespace) {
					for (int k = -1; k <= 1; k++) {
						for (int l = -1; l <= 1; l++) {
							if (((k==0 || l==0) && !(k==0 && l==0))) {
								try {
									if (vertices[x+k][y+l].isInFreespace) {
										vertices[x][y].addNeighbor(vertices[x+k][y+l]);
									}
								} catch (Exception e) { //out of matrix -->ignore			
								}
							}
						}
					}
				}
			}
		}
	}
	
	private Stack<Vertex> findPath(Vertex startVertex, Vertex endVertex){
		for (int y = 0; y < vertices[0].length; y++) {
			for (int x = 0; x < vertices.length; x++) {
				vertices[x][y].resetForDijkstra();
			}
		}
		PriorityQueue<Vertex> q = new PriorityQueue<Vertex>(); //Liste �erreichbar� (Q)
		q.add(startVertex);
		ArrayList<Vertex> f = new ArrayList<Vertex>(); //List �fertig� (F)
		while(!q.isEmpty()){ //Solange Knoten erreichbar [Q nicht leer]
			Vertex v = q.poll(); //v = Entferne Knoten mit kleinsten Kosten k aus Q
			for(Vertex n:v.neighbors){ //F�r alle von v ausgehenden Kanten (v, n)
				if(!f.contains(n)){ //Falls n nicht fertig [in Liste F]
					if(!q.contains(n)){ //Falls n bisher nicht erreichbar [nicht in Q]
						n.tempCostForDyksta=Double.MAX_VALUE; //n ist erreichbar mit Kosten unenedlich [n zu Q hinzuf�gen]
						q.add(n);
					}
					if(v.tempCostForDyksta+1<n.tempCostForDyksta){ //Falls k + Kantengewicht(v,n) < Kosten von n in Q?
						//Todo +1 --> to cost
						n.tempCostForDyksta=v.tempCostForDyksta + n.cost; //Kosten von n in Q = k + Kantengewicht(v,n)
						n.pointerToPrevious=v; //Setze v als Vorg�nger von n
					}
					
				}				
				f.add(v); //v ist fertig [v zu F hinzuf�gen]			
			}
		}
		//generate the path
		Stack<Vertex> path = new Stack<Vertex>();
		path.push(new Vertex(info.getCurrentCheckpoint().getX(), info.getCurrentCheckpoint().getY()));
		Vertex current = endVertex;
		//TODO NullpointerException
		while(current.pointerToPrevious!=null || current !=startVertex){
			path.push(current);
			current=current.pointerToPrevious;
		}
		path = optimzePath(path);
		path = addVerticesBetween(path);
		path = addVerticesBetween(path);
		return path;
	}	
	
	private Stack<Vertex> addVerticesBetween(Stack<Vertex> path) {
		Stack<Vertex> optimizedPath = new Stack<Vertex>(); //the better path
		while (path.size()>1){
			Vertex current = path.pop();
			optimizedPath.push(current);
			if(path.peek()!=null){
				//findBetween
				Vertex next = path.peek();
				Vertex half = new Vertex((current.x+next.x)/2, (current.y+next.y)/2);
				Vertex firstQ = new Vertex((current.x+half.x)/2, (current.y+half.y)/2);
				Vertex thirdQ = new Vertex((half.x+next.x)/2, (half.y+next.y)/2);
				Vertex oneEighths = new Vertex((current.x+firstQ.x)/2, (current.y+firstQ.y)/2);
				Vertex threeEighths = new Vertex((firstQ.x+half.x)/2, (firstQ.y+half.y)/2);
				Vertex fiveEighths = new Vertex((half.x+thirdQ.x)/2, (half.y+thirdQ.y)/2);
				Vertex sevenEighths = new Vertex((thirdQ.x+next.x)/2, (thirdQ.y+next.y)/2);
				optimizedPath.push(oneEighths);
				optimizedPath.push(firstQ);
				optimizedPath.push(threeEighths);
				optimizedPath.push(half);
				optimizedPath.push(fiveEighths);
				optimizedPath.push(thirdQ);
				optimizedPath.push(sevenEighths);				
			}
		}
		return flipStack(optimizedPath);
	}

	private Stack<Vertex> optimzePath(Stack<Vertex> pathToOptimize){		
		pathToOptimize=flipStack(pathToOptimize);		
		Stack<Vertex> optimizedPath = new Stack<Vertex>(); //the better path
		Vertex current = pathToOptimize.peek(); //should be the starting point
		optimizedPath.push(current); //add them allready as a correct path vertex
		Vertex candidate;
		try {
			candidate = pathToOptimize.pop();
		} catch (Exception e) {
			candidate = null;
		}
		Vertex lastCandidate = current;
		while(candidate!=null){
			//System.out.println(candidate);
			if(isConectionInFreespace(current,candidate, WidthForOptimization)
					&& isConectionNotInSlowArea(current,candidate, WidthForOptimization)){
				//there is a connection in freespace
				// --> Change Pointer one step further
				lastCandidate = candidate;
				try {
					candidate = pathToOptimize.pop();
				} catch (Exception e) {
					candidate=null;
				}
			}
			else{
				//there is no connection between current and candidate
				//-->save lastCandidate
				//update pointer
				optimizedPath.push(lastCandidate);
				current=lastCandidate;
				lastCandidate = candidate;				
			}
		}
		optimizedPath.push(lastCandidate);
		return optimizedPath;
	}
	
	private Stack<Vertex> flipStack (Stack<Vertex> inputStack){
		Stack<Vertex> inutStackCopy = (Stack<Vertex>) inputStack.clone();
		Stack<Vertex> toReturn = new Stack<Vertex>();
		while(inutStackCopy.size()!=0){
			toReturn.push(inutStackCopy.pop());
		}
		return toReturn;
	}

	private boolean isConectionInFreespace(Vertex current, Vertex candidate, double widthForBoundingPolygon) {	
		Vector vecToCandidate = new Vector(candidate.x - current.x, candidate.y - current.y);			
		double[] x = {	current.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX),
				current.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
				candidate.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
				candidate.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX)
				};
		double[] y = {	current.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY),
				current.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),
				candidate.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),	
				candidate.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY)
				};
		Path2D polygonPath = new Path2D.Double();
		polygonPath.moveTo(x[0], y[0]);
		for(int i = 0; i < x.length; ++i) {
		   polygonPath.lineTo(x[i], y[i]);}
		polygonPath.closePath();
		Area polygonPathAsArea = new Area(polygonPath);
		for(int i = 0; i<freeSpaceAreas.length; i++){
			Area copy = (Area) polygonPathAsArea.clone();
			copy.intersect(freeSpaceAreas[i]);
			if(copy.isEmpty()){
				//There is space!
			}
			else{
				//there is no space
				return false;
			}
		}
		return true;		
	}

	private boolean isConectionNotInSlowArea(Vertex current, Vertex candidate, double widthForBoundingPolygon) {	
		Vector vecToCandidate = new Vector(candidate.x - current.x, candidate.y - current.y);			
		double[] x = {	current.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX),
				current.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
				candidate.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
				candidate.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX)
				};
		double[] y = {	current.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY),
				current.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),
				candidate.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),	
				candidate.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY)
				};
		Path2D polygonPath = new Path2D.Double();
		polygonPath.moveTo(x[0], y[0]);
		for(int i = 0; i < x.length; ++i) {
		   polygonPath.lineTo(x[i], y[i]);}
		polygonPath.closePath();
		Area polygonPathAsArea = new Area(polygonPath);
		for(int i = 0; i<slowZoneAreas.length; i++){
			Area copy = (Area) polygonPathAsArea.clone();
			copy.intersect(slowZoneAreas[i]);
			if(copy.isEmpty()){
				//There is space!
			}
			else{
				//there is no space
				return false;
			}
		}
		return true;		
	}
	
	private boolean isVertexAreaInFreespace(double x, double y) {
		for(int i = 0; i<freeSpaceAreas.length; i++){
			if(freeSpaceAreas[i].intersects(x-cellDim/2, y-cellDim/2,cellDim,cellDim)){
				return false;
			}
		}
		return true;
	}
	
	private boolean isVertexAreaInFastZone(double x, double y) {
		for(int i = 0; i<fastZoneAreas.length; i++){
			if(fastZoneAreas[i].intersects(x-cellDim/2, y-cellDim/2,cellDim,cellDim)){
				return true;
			}
		}
		return false;
	}
	
	private boolean isVertexAreaInSlowZone(double x, double y) {
		for(int i = 0; i<slowZoneAreas.length; i++){
			if(slowZoneAreas[i].intersects(x-cellDim/2, y-cellDim/2,cellDim,cellDim)){
				return true;
			}
		}
		return false;
	}
	
	private boolean isCarInSlowZone() {
		for(int i = 0; i<slowZoneAreas.length; i++){
			if(slowZoneAreas[i].intersects(info.getX()-0.5, info.getY()-0.5, 1, 1)){
				return true;
			}
		}
		return false;
	}
	
	private boolean isCarInFastZone() {
		for(int i = 0; i<fastZoneAreas.length; i++){
			if(fastZoneAreas[i].intersects(info.getX()-0.5, info.getY()-0.5, 1, 1)){
				return true;
			}
		}
		return false;
	}
	
	private Vertex getNextReachable() {
		double borderRadius = RADIUSFORGETTINGNEXTREACHABLEVERTEX;
		
		double CheckPointDistanceToMePow = (info.getX()-info.getCurrentCheckpoint().x)*(info.getX()-info.getCurrentCheckpoint().x)
				+(info.getY()-info.getCurrentCheckpoint().y)*(info.getY()-info.getCurrentCheckpoint().y);
		//if checkpoint is visible
		if(isConectionInFreespace(
				new Vertex(info.getX(), info.getY()),
				new Vertex(info.getCurrentCheckpoint().x, info.getCurrentCheckpoint().y),
				WidthForLookAhead) 
				&& CheckPointDistanceToMePow < borderRadius*borderRadius*1.3				
				){
			return new Vertex(info.getCurrentCheckpoint().x, info.getCurrentCheckpoint().y);
		}
		
		Stack<Vertex> copyPath = (Stack<Vertex>) pathToDest.clone();
		copyPath = flipStack(copyPath);
		while(copyPath.size()!=0){
			Vertex candidate = copyPath.pop();
			double distanceToMePow = (info.getX()-candidate.x)*(info.getX()-candidate.x)+(info.getY()-candidate.y)*(info.getY()-candidate.y);
			if(isConectionInFreespace(new Vertex(info.getX(),info.getY()),candidate, WidthForLookAhead) 
					&& distanceToMePow < borderRadius*borderRadius){
				return candidate;
			}
		}
		borderRadius*=2;
		copyPath = (Stack<Vertex>) pathToDest.clone();
		copyPath = flipStack(copyPath);
		while(copyPath.size()!=0){
			Vertex candidate = copyPath.pop();
			double distanceToMePow = (info.getX()-candidate.x)*(info.getX()-candidate.x)+(info.getY()-candidate.y)*(info.getY()-candidate.y);
			if(isConectionInFreespace(new Vertex(info.getX(),info.getY()),candidate, WidthForLookAhead) 
					&& distanceToMePow < borderRadius*borderRadius){
				return candidate;
			}
		}
		
		borderRadius*=2;
		copyPath = (Stack<Vertex>) pathToDest.clone();
		copyPath = flipStack(copyPath);
		while(copyPath.size()!=0){
			Vertex candidate = copyPath.pop();
			double distanceToMePow = (info.getX()-candidate.x)*(info.getX()-candidate.x)+(info.getY()-candidate.y)*(info.getY()-candidate.y);
			if(isConectionInFreespace(new Vertex(info.getX(),info.getY()),candidate, WidthForLookAhead) 
					&& distanceToMePow < borderRadius*borderRadius){
				return candidate;
			}
		}
		
		return pathToDest.peek();
	}
		
	public Acceleration update(boolean arg0) {		
		long startTime = System.currentTimeMillis();
		
		try { // To avoid a disqualification - you know murphy's law
			updateMyPositionVertex();
			
			if (arg0) { //the reset
				updateCheckpointAsVertex();
				//WidthForOptimization+=10;
				pathToDest=findPath( myPositionVertex, checkpointVertex);
				currentDestination = pathToDest.peek();
				toX = currentDestination.x;
				toY = currentDestination.y;
			}
			if(pathToDest==null){
				pathToDest=findPath( myPositionVertex, checkpointVertex);
				currentDestination = pathToDest.peek();
				toX = currentDestination.x;
				toY = currentDestination.y;
			}
			else{		
				if (info.getCurrentCheckpoint().x != lastCheckpoint.x && info.getCurrentCheckpoint().y != lastCheckpoint.y) {
					//info.getCheckpoint changed!
					//--> new Path
					lastCheckpoint = new Point(info.getCurrentCheckpoint());
					updateCheckpointAsVertex();
					pathToDest = findPath(myPositionVertex, checkpointVertex);
					currentDestination = pathToDest.peek();
					toX = currentDestination.x;
					toY = currentDestination.y;
					
				}
				//normal case --> adapt to look ahead at the stack
				Vertex nextReachableVertex = getNextReachable();
				toX = nextReachableVertex.x;
				toY = nextReachableVertex.y;
			}
			
			Vector velocity = new Vector(info.getVelocity().x, info.getVelocity().y).normalize();
			Vector theoreticDest = new Vector(toX - info.getX(), toY - info.getY()).normalize();
			double angleBetwVeloAndDest = theoreticDest.AngleBetweenWithSig(velocity);
			
			if(info.getVelocity().length() > 0.1 && Math.abs(angleBetwVeloAndDest) < MAXANGLEBETWEENVELOCITYANDDESTFORSLOWDOWN
					//and is not in slow zone
					&& !isCarInFastZone()
					){
				//--> vexDest = veloxcity an theoreticDes spiegeln;			
				vecDestination = theoreticDest.rotate(-1*angleBetwVeloAndDest).normalize();
			}
			else{
				//old -->Behaviour without using the velocityVector for Seek Behaviour
				vecDestination = new Vector(toX - info.getX(), toY - info.getY()).normalize();				
			}
			
			//vecDestination = new Vector(toX - info.getX(), toY - info.getY()).normalize();		
			
			orientationDestination = Math.atan2(vecDestination.dY, vecDestination.dX);
			vecOrientation=new Vector(Math.cos(info.getOrientation()), Math.sin(info.getOrientation()));
			angleBetweenOrientationAndDestination=vecDestination.AngleBetween(vecOrientation);
			
			calculatedRotAcc = allign();		
			calculatedAcc = arrive(toX,toY);
		} catch (Exception e) {
			// in case of murphy's law
			System.out.println("Murphys Law");
			Random random = new Random();
			calculatedAcc+=((float)random.nextInt(1000)-500)/10000;
			calculatedRotAcc+=((float)random.nextInt(1000)-500)/10000;
		}				
//		long timeToCalc
//		System.out.println("Time: "+(System.currentTimeMillis()-startTime)+" ms");
		return new Acceleration((float) calculatedAcc ,(float) calculatedRotAcc);
	}

	private double allign() {
		double wunschdrehgeschwindigkeit = 0;
		if(angleBetweenOrientationAndDestination<TOLERANZWINKEL){
			//done
		}
		else if(angleBetweenOrientationAndDestination < ABBREMSWINKEL){
			wunschdrehgeschwindigkeit = (orientationDestination - info.getOrientation())*info.getMaxAngularVelocity()/ABBREMSWINKEL;
		}else{
			wunschdrehgeschwindigkeit = Math.signum(vecDestination.dotProduct(new Vector(vecOrientation.dY*(-1),vecOrientation.dX)))
					*info.getMaxAngularAcceleration();
		}
		double drehBeschleunigung = (wunschdrehgeschwindigkeit - info.getAngularVelocity())/WUNSCHDREHZEIT;
		return drehBeschleunigung;
	}

	private double arrive(double tox, double toy) {
		double distanceToCheckpoint = getDistance(info.getX(), info.getY(), tox, toy);
		double wunschgeschwindigkeit = 28;
		if(distanceToCheckpoint < ZIELRADIUS){
			//done
		}
		else if(distanceToCheckpoint < ABBREMSRADIUS){
//			wunschgeschwindigkeit = distanceToCheckpoint * info.getMaxVelocity() / ABBREMSRADIUS;
			wunschgeschwindigkeit = distanceToCheckpoint * adjustWunschgeschwindigkeit(angleBetweenOrientationAndDestination) / ABBREMSRADIUS;
		}
		else{
			wunschgeschwindigkeit = adjustWunschgeschwindigkeit(angleBetweenOrientationAndDestination); //28
//			wunschgeschwindigkeit=1000;
		}
		double beschleunigung = (wunschgeschwindigkeit - info.getVelocity().length())/WUNSCHABBREMSZEIT;
		return beschleunigung;
	}
	
	private double adjustWunschgeschwindigkeit(double angle){
//		return 28/Math.pow((5*angle+1),2)+10;
		
		if(angle<=BORDERANGLEFORSLOWDOWN){
			return 1000;
		}
		else{
			return ((MINVELOCITYFORLOWDOWN-28)/(Math.PI-BORDERANGLEFORSLOWDOWN)*(angle-BORDERANGLEFORSLOWDOWN)+28);
		}
	}
	
	private double getDistance (double x1, double y1, double x2, double y2){
		return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
	}

	boolean debug = true;
	@Override
	public void doDebugStuff() {
		super.doDebugStuff();	
		if (debug) {
			showAdjecyMatrice();
			GL11.glBegin(GL11.GL_LINES);
			//Face
			GL11.glColor3f(1, 1, 1); //Wei�
			GL11.glVertex2f(info.getX(), info.getY());
			GL11.glVertex2d(info.getX() + 15 * Math.cos(info.getOrientation()),	info.getY() + 15 * Math.sin(info.getOrientation()));
			//Checkpoint
			GL11.glColor3f(1, 0, 1); //lila
			GL11.glVertex2f(info.getX(), info.getY());
			GL11.glVertex2d(info.getX() + 15 * vecDestination.dX, info.getY() + 15 * vecDestination.dY);
			//Velocity
			GL11.glColor3f(1, 0, 0); //rot
			GL11.glVertex2f(info.getX(), info.getY());
			GL11.glVertex2d(info.getX() + 1 * info.getVelocity().x, info.getY() + 1 * info.getVelocity().y);			
			GL11.glEnd();	
			drawLookAhead(new Vertex(info.getX(), info.getY()), new Vertex(toX, toY));			
		}
	}

	private void drawLookAhead(Vertex current, Vertex candidate) {
		Vector vecToCandidate = new Vector(candidate.x - info.getX(), candidate.y - info.getY());			
		double widthForBoundingPolygon = WidthForLookAhead;

		double[] x = {	current.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX),
						current.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
						candidate.x + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dX),
						candidate.x + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dX)
						};
		double[] y = {	current.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY),
						current.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),
						candidate.y + (vecToCandidate.rotateLeft().normalize().scale(widthForBoundingPolygon).dY),	
						candidate.y + (vecToCandidate.rotateRight().normalize().scale(widthForBoundingPolygon).dY)
						};
				
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.5f,1.0f,0.5f);
		GL11.glVertex2d(x[0],+y[0]);
		GL11.glVertex2d(x[1],y[1]);
		GL11.glEnd();
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.5f,1.0f,0.5f);
		GL11.glVertex2d(x[1],y[1]);
		GL11.glVertex2d(x[2],y[2]);
		GL11.glEnd();
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.5f,1.0f,0.5f);
		GL11.glVertex2d(x[2],y[2]);
		GL11.glVertex2d(x[3],y[3]);
		GL11.glEnd();
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.5f,1.0f,0.5f);
		GL11.glVertex2d(x[3],y[3]);
		GL11.glVertex2d(x[0],y[0]);
		GL11.glEnd();
	}
	
	private void showAdjecyMatrice() {
		for (int y = 0; y < vertices[0].length; y++) {
			for (int x = 0; x < vertices.length; x++) {
				if(vertices[x][y].isInFreespace){
					if(vertices[x][y].isInFastZone){
						drawSquare(vertices[x][y], Color.GREEN);
					}
					else if(vertices[x][y].isInSlowZone){
						drawSquare(vertices[x][y], Color.RED);
					}
					else{
						drawSquare(vertices[x][y], Color.LIGHT_GRAY);
					}
//					for(Vertex neighbor:vertices[x][y].neighbors){
//						GL11.glBegin(GL11.GL_LINES);
//						GL11.glColor3f(0.9f,0.9f,0.9f);
//						GL11.glVertex2d(vertices[x][y].x,vertices[x][y].y);
//						GL11.glVertex2d(neighbor.x,neighbor.y);
//						GL11.glEnd();
//					}
				}
			}
		}		
		drawSquare(myPositionVertex, Color.yellow);
		//show path
		for (int i = 0; i<pathToDest.size()-1; i++){
			drawSquare(pathToDest.get(i), 1, Color.white);
			GL11.glBegin(GL11.GL_LINES);
			GL11.glColor3f(0.9f,0.1f,0.9f);
			GL11.glVertex2d(pathToDest.get(i).x,pathToDest.get(i).y);
			GL11.glVertex2d(pathToDest.get(i+1).x,pathToDest.get(i+1).y);
			GL11.glEnd();
		}
		drawSquare(currentDestination, Color.red);
	}
	
	void drawSquare(Vertex v, Color c){
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(c.getRed(),c.getGreen(),c.getBlue());					
		double offset=cellDim/2-1.5;					
		GL11.glVertex2d(v.x-offset,v.y-offset);
		GL11.glVertex2d(v.x+offset,v.y-offset);					
		GL11.glVertex2d(v.x+offset,v.y-offset);
		GL11.glVertex2d(v.x+offset,v.y+offset);					
		GL11.glVertex2d(v.x+offset,v.y+offset);
		GL11.glVertex2d(v.x-offset,v.y+offset);					
		GL11.glVertex2d(v.x-offset,v.y+offset);
		GL11.glVertex2d(v.x-offset,v.y-offset);
		GL11.glEnd();
	}
	
	void drawSquare(Vertex v, double offset, Color c){
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(c.getRed(),c.getGreen(),c.getBlue());									
		GL11.glVertex2d(v.x-offset,v.y-offset);
		GL11.glVertex2d(v.x+offset,v.y-offset);					
		GL11.glVertex2d(v.x+offset,v.y-offset);
		GL11.glVertex2d(v.x+offset,v.y+offset);					
		GL11.glVertex2d(v.x+offset,v.y+offset);
		GL11.glVertex2d(v.x-offset,v.y+offset);					
		GL11.glVertex2d(v.x-offset,v.y+offset);
		GL11.glVertex2d(v.x-offset,v.y-offset);
		GL11.glEnd();
	}
}