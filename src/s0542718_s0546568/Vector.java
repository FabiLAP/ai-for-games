package s0542718_s0546568;

import java.lang.Math;
import java.util.ArrayList;

public class Vector {

  public double dX;
  public double dY;

  public Vector() {
     dX = dY = 0.0;
  }

  public Vector( double dX, double dY ) {
     this.dX = dX;
     this.dY = dY;
  }
   
  public String toString() {
     return "Vector(" + dX + ", " + dY + ")";
  }

  public double length() {
     return Math.sqrt ( dX*dX + dY*dY );
  }

  public Vector add( Vector v1 ) {
      Vector v2 = new Vector( this.dX + v1.dX, this.dY + v1.dY );
      return v2;
  }

  public Vector sub( Vector v1 ) {
      Vector v2 = new Vector( this.dX - v1.dX, this.dY - v1.dY );
      return v2;
  }

  public Vector scale( double scaleFactor ) {
      Vector v2 = new Vector( this.dX*scaleFactor, this.dY*scaleFactor );
      return v2;
  }

  public Vector normalize() {
     Vector v2 = new Vector();
     double length = Math.sqrt( this.dX*this.dX + this.dY*this.dY );
     if (length != 0) {
       v2.dX = this.dX/length;
       v2.dY = this.dY/length;
     }
     return v2;
  }   

  public double dotProduct ( Vector v1 ) {
	  //System.out.println(this.dX*v1.dX + this.dY*v1.dY);
       return this.dX*v1.dX + this.dY*v1.dY;
  }
  
  public double AngleBetween (Vector v){
	  return Math.acos(this.dotProduct(v)/(this.length()*v.length()));
  }
  
  public double AngleBetweenWithSig (Vector v){
	  if( Math.signum(this.dotProduct(new Vector(-1*v.dY,v.dX))) <= -1 ){ //left
		  return Math.acos(this.dotProduct(v)/(this.length()*v.length()));
	  }
	  else{ //right
		  return (-1)*Math.acos(this.dotProduct(v)/(this.length()*v.length()));
	  }
  }
  
  public Vector rotateRight(){
	  return new Vector(this.dY, (-1)*this.dX);
  }
  
  public Vector rotateLeft(){
	  return new Vector((-1)*this.dY, this.dX);
  }
  
  public Vector rotate (double alpha){
	  double newX = this.dX*Math.cos(alpha)-this.dY*Math.sin(alpha);
	  double newY = this.dX*Math.sin(alpha)+this.dY*Math.cos(alpha);
	  return new Vector(newX,newY);
  }
//
//  
//	public static void main(String[] args) {
//	Vector v1=new Vector(3,0);
//	Vector v2=new Vector(0,3);
//	Vector v3=new Vector(0,-3);
//	Vector v4=new Vector(-3,0);
//	Vector v5=new Vector(0,3);
//
//	System.out.println(v1.rotate(Math.PI));
//	System.out.println(v1.rotate(Math.PI/2));
//	System.out.println(v1.rotate(Math.PI/4));
//	System.out.println(v1.rotate(0));
//	System.out.println(v1.rotate(-Math.PI/4));
//	System.out.println(v1.rotate(-Math.PI/2));
//	System.out.println(v1.rotate(-Math.PI));
//	
//	System.out.println();
//	System.out.println(v1.dotProduct(new Vector(-1*v2.dY,v2.dX)));
//	System.out.println(v2.dotProduct(new Vector(-1*v1.dY,v1.dX)));
//	
//	System.out.println();
//	System.out.println(v1.AngleBetween(v2));
//	System.out.println(v2.AngleBetween(v1));
//	System.out.println();
//	System.out.println(v1.AngleBetweenWithSig(v2));
//	System.out.println(v2.AngleBetweenWithSig(v1));
//	
//	System.out.println();
//	System.out.println(Math.signum(-2));
//	System.out.println(Math.signum(-0.5));
//	System.out.println(Math.signum(0));
//	System.out.println(Math.signum(1));
//	System.out.println(Math.signum(2));
//	
//	System.out.println();	
//	System.out.println("##### left");
//	System.out.println(v1+" to "+v1.rotateLeft());
//	System.out.println(v3+" to "+v3.rotateLeft());
//	System.out.println(v4+" to "+v4.rotateLeft());
//	System.out.println(v5+" to "+v5.rotateLeft());	
//	System.out.println("##### right");
//	System.out.println(v1+" to "+v1.rotateRight());
//	System.out.println(v3+" to "+v3.rotateRight());
//	System.out.println(v4+" to "+v4.rotateRight());
//	System.out.println(v5+" to "+v5.rotateRight());
//}
}
